package ru.t1.kharitonova.tm.util;

import ru.t1.kharitonova.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner scanner = new Scanner(System.in);

    static String nextLine(){
        return scanner.nextLine();
    }

    static Integer nextNumber(){
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }
}
