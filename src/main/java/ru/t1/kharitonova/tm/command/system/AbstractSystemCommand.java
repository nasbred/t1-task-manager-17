package ru.t1.kharitonova.tm.command.system;

import ru.t1.kharitonova.tm.api.service.ICommandService;
import ru.t1.kharitonova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService(){
        return serviceLocator.getCommandService();
    }

}
