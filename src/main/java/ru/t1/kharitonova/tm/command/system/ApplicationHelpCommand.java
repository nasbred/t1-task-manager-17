package ru.t1.kharitonova.tm.command.system;

import ru.t1.kharitonova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands= getCommandService().getTerminalCommands();
        for (final AbstractCommand command: commands) {
            System.out.println(command);
        }
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Display list of terminal commands.";
    }

}
