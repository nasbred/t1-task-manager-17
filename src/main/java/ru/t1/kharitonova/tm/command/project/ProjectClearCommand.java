package ru.t1.kharitonova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand{

    @Override
    public String getDescription() {
        return "Clear project.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectService().clear();
    }

}
