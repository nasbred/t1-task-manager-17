package ru.t1.kharitonova.tm.command.task;

import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand{

    @Override
    public String getDescription() {
        return "Bind task to project.";
    }

    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
