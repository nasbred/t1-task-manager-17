package ru.t1.kharitonova.tm.component;

import ru.t1.kharitonova.tm.api.repository.ICommandRepository;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.command.project.*;
import ru.t1.kharitonova.tm.command.system.*;
import ru.t1.kharitonova.tm.command.task.*;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kharitonova.tm.exception.system.CommandNotSupportedException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.repository.CommandRepository;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.service.*;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    private void initDemoData() {
        projectService.add(new Project("TEST PROJECT 1", Status.IN_PROGRESS));
        projectService.add(new Project("TEST PROJECT 3", Status.NOT_STARTED));
        projectService.add(new Project("TEST PROJECT 2", Status.IN_PROGRESS));
        projectService.add(new Project("TEST PROJECT 4", Status.COMPLETED));

        taskService.create("TEST TASK 1", "DESCRIPTION 1");
        taskService.create("TEST TASK 3", "DESCRIPTION 3");
        taskService.create("TEST TASK 4", "DESCRIPTION 4");
        taskService.create("TEST TASK 2", "DESCRIPTION 2");
    }

    public void run(String[] args) {
        runArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void runArguments(final String[] arg) {
        if (arg == null || arg.length == 0) return;
        for (String s : arg) {
            runArgument(s);
        }
        System.exit(0);
    }

    private void runCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void runArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
                }
        });
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

}
